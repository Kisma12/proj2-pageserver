from flask import Flask, render_template, request, abort
import os

app = Flask(__name__)

@app.route("/")
def index():
    fileName = request.args.get("nameRequest")
    if fileName == None:
        fileName = "Empty"
    return render_template('index.html', nameRequest=fileName)

@app.route("/<path:name>")
def namedPath(name):
    if ".." in name or "//" in name or "~" in name:
        abort(403)
    elif not (name.endswith(".html") or name.endswith(".css")):
        abort(403)
    elif os.path.exists("./templates/" + name):
        return render_template(name, nameRequest=name)
    else:
        abort(404)

@app.errorhandler(403)
def error_403(e):
    return render_template('403.html'), 403

@app.errorhandler(404)
def error_404(e):
    return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
